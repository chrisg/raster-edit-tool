import os
import sys
import tempfile
import arcpy
import pythonaddins

has_spatial = arcpy.CheckExtension('Spatial') == 'Available'

try:
    from osgeo import gdal, ogr, osr
    has_gdal = True
except:
    try:
        sys.path.append(os.path.join(os.path.dirname(__file__), 'site-packages'))
        os.environ['PATH'] = os.path.join(os.path.dirname(__file__), 'site-packages', 'osgeo') + ';' + os.environ['PATH']
        from osgeo import gdal, ogr, osr
        has_gdal = True
    except:
        has_gdal = False


class InitializeButton(object):
    """Implementation for RasterEdit_addin.init_button (Button).

    This button exists because I wanted the toolbar to populate itself
    when loaded if there were already layers loaded into the data frame,
    but the toolbar controls aren't globally available at that point, so
    any code in an item's __init__() function that referenced another
    toolbar item ended up crashing. I tried a few convoluted solutions,
    but none worked right and I finally decided that it was a waste of
    time trying to figure out something that would work, so I went with
    this solution even though I don't especially like it.
    """
    def __init__(self):
        self.enabled = has_spatial or has_gdal
        self.checked = False

    def onClick(self):
        """Syncs contents of raster_combobox to the loaded layers."""
        raster_combobox.items = []
        extension.openDocument()


class MethodComboBox(object):
    """Implementation for RasterEdit_addin.method_combobox (ComboBox).

    This combo box allows the user to select ArcPy or GDAL (if installed)
    for the processing.
    """
    def __init__(self):
        self.editable = True
        self.dropdownWidth = 'WWWWWW'
        self.width = 'WWWWWW'
        self.items = []
        if has_spatial:
            self.items.append('ArcPy')
            self.value = 'ArcPy'
        if has_gdal:
            self.items.append('GDAL')
            self.value = 'GDAL'
        self.enabled = len(self.items) > 1
        if not self.items:
            msg = "You don't have a Spatial Analyst license or a GDAL install, so Raster Edit won't work."
            pythonaddins.MessageBox(msg, 'Raster Edit')

    def onSelChange(self, selection):
        pass
    def onEditChange(self, text):
        pass
    def onFocus(self, focused):
        pass
    def onEnter(self):
        pass
    def refresh(self):
        pass


class PixelValueComboBox(object):
    """Implementation for RasterEdit_addin.value_combobox (ComboBox).

    This combo box allows the user to select a pixel value to assign to the
    pixels of interest.
    """
    def __init__(self):
        self.items = []
        self.editable = True
        self.enabled = False
        self.dropdownWidth = 'WWWW'
        self.width = 'WWWW'
        self.value = ''

    def onSelChange(self, selection):
        """Enables poly_tool when the user selects a pixel value."""
        poly_tool.enabled = True

    def onEditChange(self, text):
        """Enables poly_tool if the user enters a valid pixel value."""
        try:
            # This will cause an exception if the text can't be converted
            # to an integer.
            value = int(text)

            # Check that the converted integer is the same as the entered
            # value (for example, 10.4 will be converted to the integer 10,
            # but that's not what the user entered).
            is_int = str(value) == text

            # Check that the entered value is either non-negative or the
            # raster being edited is signed (so negative values are okay).
            is_valid = value >= 0 or raster_combobox.signed

            # Enable or disable poly_tool based on if it's a valid integer.
            poly_tool.enabled = is_int and is_valid
        except:
            # Disable poly_tool because the entered value can't be
            # converted to integer.
            poly_tool.enabled = False

    def onFocus(self, focused):
        pass
    def onEnter(self):
        pass

    def refresh(self):
        """Make the dropdown width large enough to display pixel values."""

        # Enable poly_tool if the selection is a pixel value.
        poly_tool.enabled = self.value != ''

        # Only need to do this if there are values in the list.
        if len(self.items):

            # Get the max width of all pixel values.
            max_width = max([len(str(i)) for i in self.items])

            # Change the dropdown width if the max is greater than the
            # current width.
            if max_width > len(self.dropdownWidth):
                self.dropdownWidth = 'W' * max_width


class PolygonTool(object):
    """Implementation for RasterEdit_addin.poly_tool (Tool)"""
    def __init__(self):
        self.enabled = False
        # The only way to allow the user to draw a polygon is to have them
        # draw a line and then close it and turn it into a polygon.
        self.shape = "Line"
        # Use a crosshair cursor.
        self.cursor = 3

    def onMouseDown(self, x, y, button, shift):
        pass
    def onMouseDownMap(self, x, y, button, shift):
        pass
    def onMouseUp(self, x, y, button, shift):
        pass
    def onMouseUpMap(self, x, y, button, shift):
        pass
    def onMouseMove(self, x, y, button, shift):
        pass
    def onMouseMoveMap(self, x, y, button, shift):
        pass
    def onDblClick(self):
        pass
    def onKeyDown(self, keycode, shift):
        pass
    def onKeyUp(self, keycode, shift):
        pass
    def deactivate(self):
        pass
    def onCircle(self, circle_geometry):
        pass

    def onLine(self, line_geometry):
        """Recodes pixels inside the polygon."""
        if method_combobox.value == 'GDAL':
            self.recode_gdal(line_geometry)
        else:
            self.recode_arcpy(line_geometry)

    def onRectangle(self, rectangle_geometry):
        pass

    def recode_arcpy(self, line):
        """Recodes pixels using ArcPy.

        Lock files aren't removed when ArcPy removes a layer from a data
        frame, so I ran into problems overwriting things. Instead, it saves
        a new raster and keeps the new one as the editable one. With each
        iteration, the previous temp raster is removed from the project so
        it's not cluttering things up. All of the temp rasters are deleted
        when the project is closed. This is obviously a lousy solution if
        the rasters are big and disk space is limited.
        """
        arcpy.CheckOutExtension('Spatial')

        # Remember the user's add outputs setting and then set it to True.
        add_outputs = arcpy.env.addOutputsToMap
        arcpy.env.addOutputsToMap = True

        # Get the raster layer to edit.
        raster_layer = raster_combobox.value
        raster_fn = raster_layer.dataSource
        folder, basename = os.path.split(raster_combobox.original_raster)
        new_raster_fn = arcpy.CreateUniqueName(basename, folder)

        # Convert the line to a polygon and get its vertices.
        poly = arcpy.Polygon(line.getPart(0))
        vertices = [point for point in poly.getPart(0)]

        # Create the raster object.
        raster = arcpy.Raster(raster_fn)

        # Extract the pixels in the polygon.
        pixels = arcpy.sa.ExtractByPolygon(raster, vertices)

        # Set the selected pixels to NoData.
        new_raster = arcpy.sa.Con(arcpy.sa.IsNull(pixels),
                                  raster, value_combobox.value)

        # Save the results and build the attribute table.
        new_raster.save(new_raster_fn)
        arcpy.BuildRasterAttributeTable_management(new_raster_fn)

        # Change the symbology.
        mxd = arcpy.mapping.MapDocument('CURRENT')
        df = mxd.activeDataFrame
        new_layer = arcpy.mapping.ListLayers(mxd, os.path.basename(new_raster_fn), df)[0]
        arcpy.mapping.UpdateLayer(df, new_layer, raster_layer, True)

        # Update the raster combo box.
        raster_combobox.value = new_layer
        raster_combobox.refresh()

        # Remove the old raster if it's not the original one we started with.
        if raster_layer.dataSource != raster_combobox.original_raster:
            arcpy.mapping.RemoveLayer(df, raster_layer)
            raster_combobox.temp_rasters.append(raster_fn)

        # Restore the add outputs setting.
        arcpy.env.addOutputsToMap = add_outputs
        arcpy.CheckInExtension('Spatial')

    def recode_gdal(self, line):
        """Recodes pixels using GDAL."""

        raster_layer = raster_combobox.value
        raster_fn = raster_layer.dataSource

        # Open the raster for writing and get its SRS.
        raster_ds = gdal.Open(raster_fn, True)
        srs = osr.SpatialReference(raster_ds.GetProjection())
        has_overviews = raster_ds.GetRasterBand(1).GetOverviewCount()

        # Create a polygon from the line.
        ring = ogr.Geometry(ogr.wkbLinearRing)
        for point in line.getPart(0):
            ring.AddPoint(point.X,  point.Y)
        poly = ogr.Geometry(ogr.wkbPolygon)
        poly.AddGeometry(ring)
        poly.CloseRings()

        # Create a temporary polygon layer.
        poly_ds = ogr.GetDriverByName('Memory').CreateDataSource('temp')
        poly_lyr = poly_ds.CreateLayer('temp', srs, ogr.wkbPolygon)

        # Add the polygon to the temporary layer.
        row = ogr.Feature(poly_lyr.GetLayerDefn())
        row.SetGeometry(poly)
        poly_lyr.CreateFeature(row)

        # Burn the polygon into the raster using the new pixel value.
        gdal.RasterizeLayer(raster_ds, [1], poly_lyr, burn_values=[value_combobox.value])

        # Close the datasets.
        del poly_ds, raster_ds

        # Build the attribute table. For some reason you have to do it twice.
        # It's like it uses the cache the first time, and then the edited
        # data the second time. Don't ask how many hours it took me to figure
        # out what was going on.
        arcpy.BuildRasterAttributeTable_management(raster_layer, 'Overwrite')
        arcpy.BuildRasterAttributeTable_management(raster_layer, 'Overwrite')
        if has_overviews:
            arcpy.BuildPyramids_management(raster_layer, resample_technique='NEAREST')


class RasterComboBox(object):
    """Implementation for RasterEdit_addin.raster_combobox (ComboBox).

    This combo box allows the user to select which raster to edit.
    """
    def __init__(self):
        self.items = []
        # There's a bug in Esri's code and editable has to be True or else
        # the combo box doesn't work at all.
        self.editable = True
        self.enabled = False
        self.dropdownWidth = 'WWWWWWWWWW'
        self.width = 'WWWWWWWWWW'
        self.value = ''
        self.original_raster = ''
        self.temp_rasters = []
        if not extension.enabled:
            pythonaddins.MessageBox('Please enable the Raster Edit extension.',
                                    'Raster Edit')

    def onSelChange(self, selection):
        """Fills the pixel value combo box when the user selects a raster."""
        if isinstance(selection, arcpy.mapping.Layer):
            self.original_raster = selection.dataSource
            value_combobox.enabled = True

            # Buld the RAT if there isn't one.
            if not arcpy.Raster(selection.dataSource).hasRAT:
                logger.debug('Building RAT')
                arcpy.BuildRasterAttributeTable_management(selection.dataSource)

            # Fill the value combo box with values from the raster's
            # attribute table. Don't use the `with` syntax because ArcPy
            # doesn't seem to implement that correctly and doesn't release
            # it when it should.
            try:
                rows = arcpy.da.SearchCursor(selection.dataSource, 'Value')
                value_combobox.items = [row[0] for row in rows]
                del rows
            except:
                # The try will fail if there is no RAT.
                value_combobox.items = []

            # Find out if the selected raster is a signed data type.
            self.signed = arcpy.Raster(selection.dataSource).pixelType.startswith('S')
        else:
            # Empty and disable the value combo box if the selection isn't
            # a layer.
            value_combobox.enabled = False
            value_combobox.items = []

        # Clear out the user's old selection and refresh.
        value_combobox.value = ''
        value_combobox.refresh()

    def onEditChange(self, text):
        pass
    def onFocus(self, focused):
        pass
    def onEnter(self):
        pass

    def refresh(self):
        """Updates combo box properties based on available items."""

        # Only enable the combo box if it has items.
        self.enabled = len(self.items) > 0

        # Remember the user's selection.
        selection = self.value

        # Clear out the selection if all layers were removed.
        if len(self.items) == 0:
            self.value = ''

        # If there's only one layer, make it the selected one and set the
        # dropdown width to the same size as the layer name.
        elif len(self.items) == 1:
            self.value = self.items[0]
            self.dropdownWidth = 'W' * len(self.value.name)

        # If there are multiple layers, sort them by name and set the
        # dropdown width to the length of the longest layer name.
        else:
            self.items.sort(key=lambda(v):v.name)
            self.dropdownWidth = 'W' * max([len(i.name) for i in self.items])

            # If the user's selection isn't in the list any more, then
            # clear out the selection.
            if not self.value in self.items:
                self.value = ''

        # If this code changed the selected item, then update accordingly.
        if self.value != selection:
            self.onSelChange(self.value)


class RasterEdit(object):
    """Implementation for RasterEdit_addin.extension (Extension).

    This extension monitors added and removed layers and automatically
    updates the layers combo box.
    """
    def __init__(self):
        self.enabled = True

    def openDocument(self):
        """Adds all raster layers to the combo box when an mxd is opened."""
        if self.toolbar_okay():
            mxd = arcpy.mapping.MapDocument('CURRENT')
            for lyr in arcpy.mapping.ListLayers(mxd, None, mxd.activeDataFrame):
                if self.is_int_raster_lyr(lyr):
                    raster_combobox.items.append(lyr)
            raster_combobox.refresh()

    def closeDocument(self):
        """Deletes tempt files and clears the combo box when an mxd is closed."""

        # Delete temporary rasters created by ArcPy.
        for fn in raster_combobox.temp_rasters:
            arcpy.Delete_management(fn)

        # If the current raster isn't the same as the original (from ArcPy
        # processing) as the user if they want to overwrite.
        if raster_combobox.value:
            original_fn = raster_combobox.original_raster
            current_fn = raster_combobox.value.dataSource
            if original_fn != current_fn:
                msg = 'Overwrite {} with {}?'.format(original_fn, current_fn)
                if pythonaddins.MessageBox(msg, 'Save raster', 4) == 'Yes':
                    try:
                        arcpy.Delete_management(original_fn)
                        arcpy.Rename_management(current_fn, original_fn)
                    except arcpy.ExecuteError as e:
                        if e.message.startswith('ERROR 000601:'):
                            msg = "ArcGIS won't let go of {}, so it can't be overwritten with {}.".format(
                                original_fn, current_fn)
                            pythonaddins.MessageBox(msg, 'Error overwriting file')

        # Empty the combo box.
        if self.toolbar_okay():
            raster_combobox.items = []
            raster_combobox.refresh()

    def itemAdded(self, new_item):
        """Adds a new layer to the combo box if it's a raster."""
        if self.toolbar_okay() and self.is_int_raster_lyr(new_item):
            raster_combobox.items.append(new_item)
            raster_combobox.refresh()

    def itemDeleted(self, deleted_item):
        """Deletes a removed layer from the combo box."""
        if self.toolbar_okay() and deleted_item in raster_combobox.items:
            raster_combobox.items.remove(deleted_item)
            raster_combobox.refresh()

    def is_int_raster_lyr(self, item):
        """Returns True if the item is an integer raster layer, False if not."""
        return (isinstance(item, arcpy.mapping.Layer) and item.isRasterLayer
                and arcpy.Raster(item.dataSource).isInteger)

    def toolbar_okay(self):
        """Returns True if the toolbar is open and available, False if not."""
        try:
            raster_combobox
            return has_spatial or has_gdal
        except:
            return False
