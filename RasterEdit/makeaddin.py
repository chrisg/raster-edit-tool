import os
import re
import zipfile

current_path = os.path.dirname(os.path.abspath(__file__))

no_gdal_out_zip_name = os.path.join(current_path, 'no_gdal',
                                    os.path.basename(current_path) + ".esriaddin")
gdal_out_zip_name = os.path.join(current_path, 'gdal',
                                 os.path.basename(current_path) + ".esriaddin")

BACKUP_FILE_PATTERN = re.compile(".*_addin_[0-9]+[.]py$", re.IGNORECASE)

def make_folder(path):
    if not os.path.exists(path):
        os.mkdir(path)

def looks_like_a_backup(filename):
    return bool(BACKUP_FILE_PATTERN.match(filename))

def make_addin(zip_name, ignore_dirs):
    with zipfile.ZipFile(zip_name, 'w', zipfile.ZIP_DEFLATED) as zip_file:
        for filename in ('config.xml', 'README.txt', 'makeaddin.py'):
            zip_file.write(os.path.join(current_path, filename), filename)
        dirs_to_add = ['Images', 'Install']
        for directory in dirs_to_add:
            for (path, dirs, files) in os.walk(os.path.join(current_path,
                                                            directory)):
                archive_path = os.path.relpath(path, current_path)
                if not any([archive_path.startswith(ignore) for ignore in ignore_dirs]):
                    found_file = False
                    for file in (f for f in files if not looks_like_a_backup(f)):
                        archive_file = os.path.join(archive_path, file)
                        print archive_file
                        zip_file.write(os.path.join(path, file), archive_file)
                        found_file = True
                    if not found_file:
                        zip_file.writestr(os.path.join(archive_path,
                                                       'placeholder.txt'),
                                          "(Empty directory)")

make_folder(os.path.join(current_path, 'no_gdal'))
make_folder(os.path.join(current_path, 'gdal'))
make_addin(no_gdal_out_zip_name, [os.path.join('Install', 'site-packages')])
make_addin(gdal_out_zip_name, [])
