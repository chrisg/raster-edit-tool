### Start on code
**Due November 27 at midnight**

You need to have a good start on your code by November 27th. Remember that the class ends the following week, so if you put everything off and don't write much code before the 27th, you'll only have a week to write all of the code for your project. And we all know how that'll work out!

Since I have your proposals and your pseudocode, I'll be able to tell how much progress you've made towards your final goal, and I want to see that you've done some serious work on it.

You'll turn this in by pushing code to your GitLab project. See the [instructions](create-repo.md) for creating a local code repo and linking it to GitLab. Once you've done that, you can push your code the same way that you pushed your homework during the first two parts of the class.
