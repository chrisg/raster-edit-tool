# Create your own GitLab project

In the first two parts of this class, I created your repositories for you. This time you're going to create it yourself and it'll be in your personal namespace instead of one I created (like python-osgeo-17).

## Create a new project

1. Log into [gitlab.com](https://gitlab.com)
2. Unless you've changed your default start page, you'll see a list of your projects and a large green button in the upper right corner that says "New Project". Click on this.
3. Give your project a name (where it says "my-awesome-project"). This can't have any spaces or special characters in it. The name should reflect what your project is about.
4. Put a short description of your project in the "Project description" box. It says it's optional, but I want you to do it.
5. Select a visibility level. If you choose Private, you'll have to give people access to it before they can see it (including me). If you choose Internal, then anyone with a GitLab account can see it and copy your code. If you choose Public, then anyone with an Internet connection can see it and copy your code.
6. Hit the green "Create project" button.
7. It'll take a second, and then you'll be taken to your project's page and it'll say "The repository for this project is empty" and you'll see a URL. You'll use this URL to upload code, just like you did for your homework repositories.

### Add me to your project

1. On your project's home page in GitLab, click on the "Settings" link in the menu on the left-hand side of the page, and then choose "Members".
2. Search for me by name (Chris Garrard, and my username is chrisg). Once you've added me with at least **Reporter** permissions, click the "Add to project" button.

![chrisg](images/chrisg-member.png) 

