# Initialize your code repo

You have two choices for creating your local code repository. You can either clone the empty repository from your GitLab project and put your code in that folder, or you can initialize a local folder as a Git repository and then tell it where to find your GitLab project. Since you should know how to clone a repository by now, these directions will walk you through initializing a new folder if you want to try it that way.

#### To use Git GUI

1. Make sure that the folder you want to use already exists. It doesn't matter if it already has code in it.
2. Open Git GUI and choose "Create New Repository".
3. Select the folder that you want to use and hit Create.
4. Your new repository will open.
5. Go to your project's home page on GitLab (not the wiki) and copy the url (the same one you would use to clone it).
6. Make sure your code repository is open in Git GUI and choose "Add..." from the "Remote" menu.
7. Paste your url into the Location box, put `origin` in the Name box, and select "Do Nothing Else Now". Then hit Add. ![origin url](images/origin-url.png)
8. Once it does it's thing, you'll be able to push to your repository and see your code online at gitlab.com.

#### To use Git Bash

1. Make sure that the folder you want to use already exists. It doesn't matter if it already has code in it.
2. Open up Git Bash in your folder.
3. Go to your project's home page on GitLab (not the wiki) and find the command line instructions for using an existing folder (these will be there if the repo is empty on gitlab.com).
4. There will be six lines of instructions, but you really only need the second and third ones right now unless you have code to push. For example, here's what it tells me to do:

This changes to the correct folder, which you don't need to do if you've opened Git Bash in the correct folder.

```
cd existing_folder
```

This initializes the local folder and tells it where to find the project on gitlab.com. These are the two you need right now (although you'd obviously use YOUR url instead of mine!).

```
git init
git remote add origin https://gitlab.com/chrisg/raster-edit-tool.git
```

This commits all files in the folder and pushes them to GitLab. If there's nothing in the folder, then there's obviously no point in doing this yet.

```
git add .
git commit -m "Initial commit"
git push -u origin master
```
