# Create a wiki

GitLab uses Markdown for its wiki pages, so that's how you'll need to write your proposal, among other things. There's documentation [here](https://gitlab.com/help/user/markdown). You can write in a text editor and then use git to sync your files to your wiki, or you can write the files on the GitLab website.

Either way, the front page is called "home". It's a good idea to put a summary and links to your other pages on this page.

For example, to create a You'll be told to put `[Proposal](proposal)` in your home page. This creates a link to another page in the wiki. The text inside the square brackets is the text that the user sees and can click on, and the text inside the parentheses is the name of the page to link to. For example, you could change the clickable link to say "My proposal" by changing the code to `[My proposal](proposal)` .

### To edit on gitlab.com

1. When you're looking at your project's page on gitlab.com, click on the "Wiki" link in the menu on the left-hand side of the page.
2. First you're going to create your Home page. Right now all you'll do is create a link to the Proposal page. To do this, add `[Proposal](proposal)` as the text for the home page, add a Commit message, and then hit the "Create page" button. ![create page](images/create-page.png)
3. Now create your proposal page by clicking on the Proposal link that you see on your home page. ![proposal page](images/proposal-page.png)
4. Use the text editor to add your proposal. You can always edit it after saving it, so it doesn't have to be perfect before you save it the first time. Notice that there's also a Preview button so you can see what it looks like before you save it.

### To edit in a text editor

You can edit .md files with any plain text editor, like Notepad, [Notepad++](https://notepad-plus-plus.org/), [Atom](https://atom.io/), or (my favorite) [Sublime Text](https://www.sublimetext.com/). Even Spyder will work. Just make sure that whatever you use doesn't change the extension to .txt or .py or some other crazy thing. (Don't use Word.)

1. When you're looking at your project's page on gitlab.com, click on the "Wiki" link in the menu on the left-hand side of the page.
2. You'll need to clone the repo to your computer. Click on the "Clone repository" link in the upper right corner of the page.
3. You'll see stuff about installing Gollum. We're not going to mess with that. Instead, just copy the URL shown in the top section.
4. Open Git GUI and select Clone Existing Repository.
5. Paste the URL from step 3 into the Source Location box.
6. For Target Directory, choose a folder on your flash drive. Once that's added, type the name of a new subfolder that doesn't exist yet at the end.
7. Hit the Clone button.
8. Enter your GitLab username and password when prompted.
9. It'll work for a minute and then a new window will open up with your repo.
10. If you had created a home page using the GitLab website then you'll see a file called home.md in your repo. If not, go ahead and create it now and put this in it: `[Proposal](proposal)`. Make sure you save it.
11. Create a file called proposal.md and put the text of your proposal in it. Use the formatting info from <https://gitlab.com/help/user/markdown>.
12. After you've saved both of your files, make sure your wiki repository is open in Git GUI, stage and commit home.md and proposal.md, and then push them to your wiki repository.
13. Now if you go back to your project's wiki on gitlab.com, you'll see your new pages.
14. Update pages by editing them in a text editor and then committing and pushing the changes. If you edit a page on the website, you'll need to use Git to pull those changes before you can push new ones.
