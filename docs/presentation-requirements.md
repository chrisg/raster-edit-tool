# Presentation requirements
**Due December 7**

1. Use graphics and/or a demo to explain to us what the problem is and why you want to write code for it. I don't care how you choose to do this. It could be with PowerPoint, a live demo, your wiki, or any other method that you feel would be effective, as long as it provides a graphical intro to the problem.
2. Provide an overview of how the code works. This needs to give us a general idea of what's happening, but please don't make it so detailed that it bores everyone to tears.
3. Show a live demo of your code in action.

These presentations don't have to be long; they just need to cover everything sufficiently. That means that some will be longer than others, but I expect them to be at least 5-10 minutes long. Definitely don't go over 20 minutes (although I doubt any of you would be tempted to do that!).
