# Project requirements
**Due December 6 in class**

## Project (35 points)
- Must be solved with Python code
- Must involve GIS data
- Must be considerably more complicated than the homework assignments
- Must solve a real problem
- Must be able to handle different data sets
    - i.e. No hardcoded filenames
- Must have some sort of user interface instead of editing the script
    - Toolbox
    - Add-in
    - Python module containing a collection of functions
    - Stand-alone script that takes command line parameters
    - Custom Python GUI
- Code repository and Wiki

### Code repository

- All of your code must be pushed to your GitLab repository.
- Code must be commented.
- Your code repository needs to have a readme.md file that contains a brief description of your tool and links to your wiki. If you want to include enough info in your readme that a user wouldn't need to read the wiki in order to learn the basics of the tool, that's even better (and that's what I'm going to do). That way, the user would only need to go to the wiki if they wanted more details.

## Wiki (15 points)

- Front page (called home) will have a description of the problem, what your tool does, how to install it, and how to use it (or links to pages that do talk about these things). This includes info about input and output data. This page will also have links to your proposal, psuedocode, and anything else such as detailed instructions or tool background.
- Proposal
- Pseudocode
- Detailed instruction or info pages are required if there's too much stuff for the front page. This would be needed if the user would need a lot of instruction or other information in order to successfully use your tool.

## Presentation (20 points)

- Use graphics and/or a demo to explain to us what the problem is and why you want to write code for it. I don't care how you choose to do this. It could be with PowerPoint, a live demo, your wiki, or any other method that you feel would be effective, as long as it provides a graphical intro to the problem.
- Provide an overview of how the code works. This needs to give us a general idea of what's happening, but please don't make it so detailed that it bores everyone to tears.
- Show a live demo of your code in action.

These presentations don't have to be long; they just need to cover everything sufficiently. That means that some will be longer than others, but I expect them to be at least 5-10 minutes long. Definitely don't go over 20 minutes (although I doubt any of you would be tempted to do that!).
