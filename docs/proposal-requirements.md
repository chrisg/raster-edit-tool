### Proposal
**Due November 13 at midnight**

You'll turn this in by creating a wiki page for your GitLab project. Make sure you've added me to your project so that I know where to look and have permissions to view it!

Write-up of your proposed project, including:

- The problem to solve
- General description of how to solve it
- Required input data
- Output data
- User interface description
    - Type of interface
    - Parameters that user needs to enter
- Any problems you might run into and how you propose to solve them

I want this to be fairly detailed and for it to be obvious that you've put some serious thought into it. If you haven't thought of any possible problems you might run into or parts of it that might hard to solve, then either you haven't given it enough thought or the project's too easy.

You can see the [example proposal](https://gitlab.com/chrisg/raster-edit-tool/wikis/proposal) for ideas, but keep in mind that the project description in the example is rather brief because it's an easy problem to explain. Chances are that your problem won't be as simple to explain as "draw a polygon to recode pixels", so you need to make sure that it's explained sufficiently.

Definitely include graphics or screenshots if they'll help explain your problem and/or solution.
