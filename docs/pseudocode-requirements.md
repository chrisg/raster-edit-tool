### Pseudocode
**Due November 20 at midnight**

You'll turn this in by creating a wiki page for your GitLab project.

[Wikipedia](https://en.wikipedia.org/wiki/Pseudocode) has this to say about pseudocode:

> Pseudocode is an informal high-level description of the operating principle of a computer program or other algorithm.

> It uses the structural conventions of a normal programming language, but is intended for human reading rather than machine reading. Pseudocode typically omits details that are essential for machine understanding of the algorithm, such as variable declarations, system-specific code and some subroutines. The programming language is augmented with natural language description details, where convenient, or with compact mathematical notation. The purpose of using pseudocode is that it is easier for people to understand than conventional programming language code, and that it is an efficient and environment-independent description of the key principles of an algorithm. It is commonly used in textbooks and scientific publications that are documenting various algorithms, and also in planning of computer program development, for sketching out the structure of the program before the actual coding takes place.

Basically what this means is that pseudocode shows exactly what the code is going to do, but in plain English.

If you look at the examples on Wikipedia, they almost look like real code. You can write yours that way, or more informally like my [example](https://gitlab.com/chrisg/raster-edit-tool/wikis/Pseudocode). Either way, you need a **extremely detailed** step-by-step outline of what your code is going to do, and you also need to include any information such as equations. If in doubt, include it!
