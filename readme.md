# An ArcGIS Python add-in for editing rasters

#### This is a sample project for the USU WILD 6900/4950 Python GIS Project class.

This is a Python add-in for ArcGIS that allows a user to manually recode pixels in a classified raster. It allows the user to draw a polygon on top of an integer raster, and all pixels that fall within the polygon are set to a new value of the user's choosing.

| Before                           | After                          |
| :------------------------------: | :----------------------------: |
| ![original](images/original.png) | ![recoded](images/recoded.png) |

See the [wiki](https://gitlab.com/chrisg/raster-edit-tool/wikis/home) for more info.

### Installation

You can pull the repo and run the `RasterEdit/makeaddin.py` script to build the add-in yourself, or you can download the current version using one of these links:

- [RasterEdit.esriaddin](https://usu.box.com/v/raster-edit-naked-current) without GDAL included
- [RasterEdit.esriaddin](https://usu.box.com/v/RasterEdit-osgeo-current) with GDAL included

Before you can install the add-in, you need to make sure that ArcMap will even let you install an unsigned add-in. To do this. Open the Add-In Manager from the Customize menu. Go to the Options tab and make sure that the radio button for "Load all Add-Ins without restrictions" is selected.

Now you can install the add-in by double-clicking on `RasterEdit.esriaddin` while ArcMap is closed. When ArcMap is started back up, the add-in will be visible in the Add-In Manager that's available through the Customize menu.

There's an associated extension, called Raster Edit, that needs to be enabled in the Extensions dialog (in the Customize menu). You also need to add the Raster Edit toolbar by selecting it from the Customize | Toolbars menu.

**GDAL notes** If you want to install and use your own version of GDAL, you need to install it for the 32-bit Python used by ArcMap, even if you also have the 64-bit geoprocessor installed.

### Usage

The Raster Edit toolbar looks like this:

![toolbar](images/toolbar_annotated.png)

Select the processing method you want to use with the dropdown list on the Raster Edit toolbar. There are two possible methods available:

1. ArcPy requires a Spatial Analyst license. This method creates intermediate rasters that are saved and loaded into your document. When the document is closed you have the option to overwrite the original raster with the current working version. This method is slower and uses more disk space than using GDAL.
2. GDAL requires that the Python osgeo package be installed and available. This method edits the rasters in place.

Integer rasters that are added to the ArcMap document will be available through the Layers dropdown list. Select which raster layer to edit.

If you loaded rasters before enabling the extension or adding the toolbar, the list of layers will be incomplete. You can update it with the Sync button.

Once you've selected a layer, the Value dropdown list will be populated with the current pixel values in the raster. You can select a value from the list or enter your own integer value to use.

Now you're ready to use the Draw tool. Draw a polygon that surrounds the pixels you want to change. Double-click to close the polygon and start the recode process. The amount of time it takes to finish depends on the size of your raster and the processing method you selected.

If you're using ArcPy, you'll see a new layer added to the map document and the toolbar will have that new layer selected in the dropdown list. At this point the original raster won't have been changed, so you'll need to edit the new one. For example, if the original raster is called veg.tif, then the temporary raster created with the first edit will be called veg0.tif and it will be selected as the editable raster in the dropdown list. Keep it as the editable raster, unless you want to lose the edits it contains. The next time you run the tool, veg0.tif will be removed from the document (but not yet from disk), and veg1.tif will be created and become the editable raster. When you close the map document you'll be asked if you want to overwrite the original raster (e.g veg.tif) with the current one (e.g. veg1.tif).

### Repo contents

The `scripts` folder contains scripts for testing the recode algorithms.

The add-in code is in the `RasterEdit` folder. Specifically, the main code file is [`RasterEdit\Install\RasterEdit_addin.py`](RasterEdit/Install/RasterEdit_addin.py).

The osgeo (GDAL) binaries and associated data are in the `RasterEdit\Install\osgeo` folder.
