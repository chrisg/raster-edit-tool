# Script to create a raster for testing things on.

import numpy as np
from osgeo import gdal

fn = r'D:\Classes\2016\raster-edit\data\test.tif'
ds = gdal.GetDriverByName('GTiff').Create(fn, 100, 100)
ds.GetRasterBand(1).WriteArray(np.random.randint(0, 10, (100, 100)))
ds.SetGeoTransform([0, 1, 0, 100, 0, -1])
