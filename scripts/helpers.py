import arcpy

def create_line(coords=((25, 25), (25, 75), (75, 75), (75, 25))):
    """ Creates a line to use for tests. """
    return arcpy.Polyline(arcpy.Array([arcpy.Point(x, y) for x, y in coords]))

def create_raster():
    """ Creates a raster to use for tests. """
    arcpy.env.overwriteOutput = True
    arcpy.CreateRandomRaster_management(out_path=r'D:\Classes\2016\raster-edit\data',
                                        out_name='test.tif',
                                        distribution='INTEGER 0 10',
                                        raster_extent='0 0 100 100',
                                        cellsize=1)

def create_shapefile():
    """ Creates a polygon shapefile using the result from create_line()."""
    fn = arcpy.CreateFeatureclass_management(out_path=r'D:\Classes\2016\raster-edit\data',
                                             out_name='poly.shp',
                                             geometry_type='POLYGON')
    with arcpy.da.InsertCursor(fn, 'SHAPE@') as inserter:
        inserter.insertRow([arcpy.Polygon(create_line().getPart(0))])
