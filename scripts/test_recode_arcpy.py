# Test script for recoding pixels using ArcPy.

import os
import arcpy
import helpers

folder = r'D:\Classes\2016\raster-edit\data'
fn = 'test.tif'
value = 5
line = helpers.create_line()

arcpy.env.workspace = folder
arcpy.CheckOutExtension('Spatial')

# Remember the user's overwrite setting and then set
# it to True
overwrite = arcpy.env.overwriteOutput
arcpy.env.overwriteOutput = True

# Create the raster object
raster = arcpy.Raster(fn)

# Create a polygon
poly = arcpy.Polygon(line.getPart(0))

# Create a list of polygon vertices
vertices = [point for point in poly.getPart(0)]

# Extract pixels in polygon
pixels = arcpy.sa.ExtractByPolygon(raster, vertices)

# Set the selected pixels to NoData
new_raster = arcpy.sa.Con(arcpy.sa.IsNull(pixels), raster, value)

# Make a backup of the original file
fn_parts = os.path.splitext(os.path.basename(fn))
backup_fn = fn_parts[0] + '_bak' + fn_parts[1]
arcpy.Delete_management(backup_fn)
arcpy.Rename_management(fn, backup_fn)

# Save the results
new_raster.save(fn)

# Build the attribute table
arcpy.BuildRasterAttributeTable_management(fn)

# Restore the overwrite setting
arcpy.env.overwriteOutput = overwrite
