# Test script for recoding pixels using GDAL.

import os
from osgeo import gdal, ogr, osr
import helpers

folder = r'D:\Classes\2016\raster-edit\data'
fn = 'test.tif'
value = 5
line = helpers.create_line()

os.chdir(folder)

# Open the raster for writing
raster_ds = gdal.Open(fn, True)

# Get the raster's SRS
srs = osr.SpatialReference(raster_ds.GetProjection())

# Create a polygon from the line
ring = ogr.Geometry(ogr.wkbLinearRing)
for point in line.getPart(0):
    ring.AddPoint(point.X,  point.Y)
poly = ogr.Geometry(ogr.wkbPolygon)
poly.AddGeometry(ring)
poly.CloseRings()

# Create a temporary polygon layer
poly_ds = ogr.GetDriverByName('Memory').CreateDataSource('temp')
poly_lyr = poly_ds.CreateLayer('temp', srs, ogr.wkbPolygon)

# Add the polygon to the temporary layer
row = ogr.Feature(poly_lyr.GetLayerDefn())
row.SetGeometry(poly)
poly_lyr.CreateFeature(row)

# Burn the polygon into the raster using the new pixel value
gdal.RasterizeLayer(raster_ds, [1], poly_lyr, burn_values=[value])

# Close the datasets
del poly_ds, raster_ds
